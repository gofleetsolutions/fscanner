# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
fscanner entry point
"""

import argparse
import json
import os
import sys

import zmq

from . import __version__
from .fscanner import FScanner

VERSION_INTRO = r'fscanner v{}'.format(__version__)


def determine_path():
    try:
        root = __file__
        if os.path.islink(root):
            root = os.path.realpath(root)
        return os.path.dirname(os.path.abspath(root))
    except:
        print("Invalid __file__ variable.")
        sys.exit()


def parse_args():
    parser = argparse.ArgumentParser(
        description='Scan nearby humans and publish to 0MQ sockets.')
    parser.add_argument(
        '-p',
        '--period',
        dest='period',
        type=float,
        default=0,
        help='Set the scan period in <sec> (default=0).')
    parser.add_argument(
        '-d',
        '--device',
        dest='device',
        type=int,
        default=0,
        help='Video device index (default=0).')
    parser.add_argument(
        '-a',
        '--addr',
        dest='address',
        action='append',
        type=str,
        help=
        '0MQ socket address to publish data, in the form ‘protocol://interface:port’. '
        'You may set this option multiple times.')
    parser.add_argument(
        '-t',
        '--topic',
        dest='topic',
        default='BFS',
        help='0MQ data topic (default=\'BFS\').')
    parser.add_argument(
        '-q',
        '--quiet',
        dest='quiet',
        action='store_true',
        help='Disable logging of scan results.')
    parser.add_argument(
        '-V', '--version', dest='show_version', action='store_true')

    return vars(parser.parse_args())


class Publisher:
    def __init__(self, addrs, topic, encoding='utf-8'):
        self._context = zmq.Context()
        self._publisher = self._context.socket(zmq.PUB)
        self._topic = topic
        self._encoding = encoding

        if isinstance(addrs, str):
            addrs = (addrs, )

        for addr in addrs:
            self._publisher.bind(addr)

    def send(self, data):
        multipart_msg = [
            part.encode(self._encoding) for part in [self._topic, data]
        ]
        self._publisher.send_multipart(multipart_msg)


def publish_writer(publisher):
    """ Create closure to publish scan result over ZeroMQ socket
    """

    def _wrapper(result):
        if publisher:
            publisher.send(json.dumps(result.asdict()))

    return _wrapper


def main():
    arguments = parse_args()
    if arguments.get('show_version'):
        print(VERSION_INTRO)
        return

    writers = list()
    topic = arguments.get('topic')
    addrs = arguments.get('address')
    publisher = None
    if addrs:
        publisher = Publisher(addrs, topic)
        writers.append(publish_writer(publisher))

    if not arguments.pop('quiet', None):
        writers.append(lambda result: print(result.asdict()))

    period = arguments.get('period')
    device_index = arguments.get('device')
    try:
        classifier_files = {"cars": "haarcascade_vehicle.xml"}
        FScanner(writers=writers, period=period).scan(classifier_files, device_index)

    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
