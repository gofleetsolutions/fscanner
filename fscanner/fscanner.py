# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.

import datetime
import json
import os
import time
from collections import namedtuple
from threading import Thread

import attr

import cv2
import numpy as np


@attr.s
class ScanResult:
    """ Single scan result
    """
    datetime = attr.ib(validator=attr.validators.instance_of(str))
    item = attr.ib(validator=attr.validators.instance_of(str))
    count = attr.ib(validator=attr.validators.instance_of(int))

    def asdict(self):
        """ Return the current object in dict format
        """
        return attr.asdict(self)


def cascade_classifier(file_name):
    """ Return the haarcascade classifier
    """
    haarcascade_file = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'data', file_name)
    return cv2.CascadeClassifier(haarcascade_file)


class FrameStream:
    """ Frame stream capture in a separate thread
    """
    def __init__(self, device_index:int = 0):
        self.frame = None
        self.stopped = False
        self.cap = cv2.VideoCapture(device_index)

    def start(self):
        """ Start the thread to read frame from the video capture object
        """
        self.stopped = False
        Thread(target=self.update, args=()).start()
        return self

    def update(self):
        """ Loop indefinitely until the thread is stopped
        """
        while not self.stopped:
            ret, frame = self.cap.read()
            if ret:
                self.frame = frame

        self.cap.release()

    def read(self):
        """ Return the frame most recently read
        """
        return self.frame

    def stop(self):
        """ Set the flag that indicate the thread should be stopped
        """
        self.stopped = True


class FScanner:
    """ Face detection and counting
    """

    def __init__(self, writers, period):
        self._writers = writers
        self._period = period

    def scan(self, classifier_files:dict, device_index:int = 0):
        """ Start scanning
        """
        classifiers = dict()
        for classifier_name, classifier_file in classifier_files.items():
            classifiers[classifier_name] = cascade_classifier(classifier_file)

        stream = FrameStream(device_index).start()

        while True:
            start = time.time()

            frame = stream.read()
            if frame is None:
                continue
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            display_text = ''

            for classifier_name, classifier in classifiers.items():
                detect_obj = classifier.detectMultiScale(gray)

                if len(detect_obj):
                    # for (x, y, w, h) in detect_obj:
                    #     cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 4)

                    detect_text = '{}: {}'.format(classifier_name, len(detect_obj))
                    print(detect_text)

                    if display_text:
                        display_text += ', '

                    display_text += detect_text

                    date_time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
                    result = ScanResult(datetime=date_time, item=classifier_name, count=len(detect_obj))

                    for writer in self._writers:
                        writer(result)

            # cv2.rectangle(frame, ((0, frame.shape[0] - 25)),
            #                       (170, frame.shape[0]), (255, 255, 255), -1)
            # cv2.putText(frame, display_text, (0, frame.shape[0] - 10),
            #             cv2.FONT_HERSHEY_TRIPLEX, 0.5, (0, 0, 0), 1)
            # cv2.imshow('output', frame)

            # if cv2.waitKey(5) == ord('q'):
            #     break

            try:
                elapsed = time.time() - start
                print('Time: {}'.format(elapsed))
                time.sleep(self._period - elapsed)

            except ValueError:
                pass

        stream.stop()
        # cv2.destroyAllWindows()


if __name__ == '__main__':
    FScanner().scan()
