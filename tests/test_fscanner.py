# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
@package test_fscanner

This package implements the fscanner unit tests
"""

import sys
import unittest
from unittest.mock import patch

from fscanner.main import parse_args

DEFAULT_ARGUMENTS = {
    'period': 0,
    'quiet': False,
    'address': None,
    'topic': 'BFS',
    'show_version': False
}


class TCFscanner(unittest.TestCase):
    """ Implements unit tests for the fscanner
    """

    def test_parse_args(self):
        """ Check the arguments parser
        """
        testargs = ["fscanner"]
        with patch.object(sys, 'argv', testargs):
            arguments = parse_args()

            self.assertEqual(arguments, DEFAULT_ARGUMENTS)
