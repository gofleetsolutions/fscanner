# fscanner
Scan for nearby humans.

Fscanner is Python3 OpenCV project that scans for nearby humans and publishes
results over a [ZeroMQ](http://zeromq.org/) socket in Json format.

# Installation

Clone the source tree:
```sh
git clone https://gitlab.com/gofleetsolutions/fscanner.git
```

Install fscanner with:
```sh
python3 setup.py install
```

# Camera configuration


# Usage
From the command line, scan and publish to ipc /tmp/fscanner:
```sh
fscanner --addr 'ipc:///tmp/fscanner'
```

# How does fscanner work?

FScanner is using OpenCV and Haar Cascade algorithms to scan for nearby humans.

Results are then published over a ZeroMQ socket in Json format:
```json
# Probe request from device
{
"datetime": "2019-01-06T13:25:45Z",
"body": "10",
"face": "2"
}
```

# Reports

* [Pylint report](https://gofleetsolutions.gitlab.io/fscanner/quality-report/pylint-report.html)
* [Coverage report](https://gofleetsolutions.gitlab.io/fscanner/coverage-report/index.html)
* [Unit tests report](https://gofleetsolutions.gitlab.io/fscanner/test-report/test_report.html)
